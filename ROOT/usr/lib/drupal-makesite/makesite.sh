#!/bin/bash

set -e

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DEFAULTS_FILE=/etc/default/drupal9_makesite
COMPOSER="sudo -u www-data composer --no-interaction"
DRUSH="drush -y"

# whether to use composer dev packages
DEV=false

FORCE=no

VERSION=^10

while getopts "dyi:e:t:v:f:" OPTION; do
  case $OPTION in
    i)
      INTERNAL_URL=$OPTARG # with protocol, e.g. https://www-test.ch.private.cam.ac.uk
      ;;
    e)
      EXTERNAL_URL=$OPTARG # with protocol, e.g. https://test.ch.cam.ac.uk
      ;;
    t)
      SITE_TITLE=$OPTARG
      ;;
    v)
      VERSION=$OPTARG # any valid composer version. Include ^, ~, etc if needed
      ;;
    f)
      FLAVOUR=$OPTARG
      ;;
    d)
      DEV=true
      ;;
    y)
      FORCE=true # Needed to build 'localhost' sites under CI. Do not use otherwise.
      ;;
    *)
      echo "Unknown option"
      exit 1
      ;;
   esac
done

if [ -z "$INTERNAL_URL" ] ; then
  echo must specify \[-i INTERNAL_URL\]
  echo e.g. https://www-test.ch.private.cam.ac.uk
  exit 1
fi

if [ -z "$SITE_TITLE" ] ; then
  echo must specify \[-t SITE_TITLE\]
  exit 1
fi

ALLOWED_FLAVOURS=("minimal" "research")

if [ -n "${FLAVOUR}" ] ; then
  if [[ ! " ${ALLOWED_FLAVOURS[@]} " =~ " $FLAVOUR " ]] ; then
    echo flavour $FLAVOUR not allowed
    echo allowed flavous: ${ALLOWED_FLAVOURS[*]}
    exit 1
  fi
fi

if [ ! -x "$(which j2)" ] ; then
  echo j2 \(jinja cli\) not found
  exit 1
fi

MYFQDN=$(hostname -f)
INTERNAL_PROTO=$(echo $INTERNAL_URL | grep :// | sed -e's,^\(.*://\).*,\1,g')
INTERNAL_NAME=$(echo ${INTERNAL_URL/$INTERNAL_PROTO/})

if [ -z "${INTERNAL_PROTO}" ] ; then
    INTERNAL_PROTO=http
fi
INTERNAL_CNAME=$(dig -t CNAME $INTERNAL_NAME +short | sed s/\.$//)

if [ -z "$INTERNAL_CNAME" ] ; then
  echo $INTERNAL_NAME is not a registered CNAME
  if [ "$FORCE" == "no" ] ; then
    exit 1
  else
    echo "but -y specified so continuing anyway"
  fi
fi

if [ "$INTERNAL_CNAME" != "$MYFQDN" ] ; then
  echo $INTERNAL_NAME is a CNAME for $INTERNAL_CNAME rather than $MYFQDN
  if [ "$FORCE" == "no" ] ; then
    exit 1
  else
    echo "but -y specified so continuing anyway"
  fi
fi

if [ -n "$EXTERNAL_URL" ] ; then
  EXTERNAL_PROTO=$(echo $EXTERNAL_URL | grep :// | sed -e's,^\(.*://\).*,\1,g')
  EXTERNAL_NAME=$(echo ${EXTERNAL_URL/$EXTERNAL_PROTO/})

  EXTERNAL_CNAME=$(dig -t CNAME $EXTERNAL_NAME +short | sed s/\.$//)

  if [ -z "$EXTERNAL_CNAME" ] ; then
    echo $EXTERNAL_NAME is not a registered CNAME
    if [ "$FORCE" == "no" ] ; then
      exit 1
    else
      echo "but -y specified so continuing anyway"
    fi
  fi
fi

USE_SSL=no
if [ "$INTERNAL_PROTO" == "https://" ] ; then
  USE_SSL=yes
fi

SITENAME=$(echo $INTERNAL_NAME | cut -d '.' -f 1 | sed s/^www-//)
SERVERSHORTNAME=$(echo $INTERNAL_NAME | cut -d '.' -f 1)

APACHECONFFILE=/etc/apache2/sites-available/${SERVERSHORTNAME}.conf

DBNAME=$(echo $SITENAME | tr '-' '_')
DBUSER=$DBNAME
SITEDIR=/var/www/drupal/$SITENAME
SETTINGSPHP=${SITEDIR}/web/sites/default/settings.php

if [ ! -f "$DEFAULTS_FILE" ] ; then
  echo $DEFAULTS_FILE not found
  exit 1
fi

source $DEFAULTS_FILE
# should be set in $DEFAULTS_FILE
if [ -z "$ENTRA_CLIENT_ID" ] ; then
  echo ENTRA_CLIENT_ID is not set
  exit 1
fi

if [ -e "$SITEDIR" ] ; then
  echo $SITEDIR already exists. Bailing out.
  exit 1
fi

if [ -e "$APACHECONFFILE" ] ; then
  echo $APACHECONFFILE already exists. Bailing out.
  exit 1
fi

if [ -f /root/.my.cnf ] ; then
  MYSQL="mysql"
else
  MYSQL="mysql --defaults-file=/etc/mysql/debian.cnf"
fi

# set up database
SQLCMDFILE=$(mktemp)

DBPASS=$(LC_ALL=C tr -dc A-Za-z0-9 2>/dev/null </dev/urandom | dd bs=12 count=1 2>/dev/null)

cat<<EOF >$SQLCMDFILE
create database $DBNAME;
grant all on $DBNAME.* to '$DBUSER'@'localhost' identified by '$DBPASS';
EOF

$MYSQL < $SQLCMDFILE
rm $SQLCMDFILE

mkdir -p /var/log/apache2/${SERVERSHORTNAME}

pushd /var/www/drupal > /dev/null

mkdir $SITEDIR
chown www-data:www-data $SITEDIR
chmod g+s $SITEDIR

$COMPOSER create-project drupal/recommended-project:${VERSION} $SITEDIR
sudo -u www-data mkdir -p $SITEDIR/web/modules/contrib

pushd $SITEDIR
$COMPOSER config repositories.$GITLAB_REPO_NAME composer $GITLAB_REPO_URL

if ${DEV} ; then
  $COMPOSER config prefer-stable
  $COMPOSER config minimum-stability dev
fi

# need drush 12.4 as of Drupal 10.2
$COMPOSER require drush/drush
$COMPOSER require ucam_drupal/ucam_profile
sudo -u www-data ./vendor/bin/drush -y site:install ucam_profile --db-url=mysql://$DBUSER:$DBPASS@localhost:3306/$DBNAME --site-name="$SITE_TITLE"

# https://www.drupal.org/project/drupal/issues/3091285
# Currently recommended workaround is #52, though with different filenames.
jq --indent 4 '.extra["drupal-scaffold"] += {"file-mapping": {"[web-root]/sites/default/default.services.yml": false, "[web-root]/sites/default/default.settings.php": false}}' composer.json > composer.json.tmp && cp composer.json.tmp composer.json && rm composer.json.tmp
# and ensure composer is up-to-date having changed drupal-scaffold settings
$COMPOSER update drupal/core-composer-scaffold
# now check our composer.json ; this script runs "set -e"
$COMPOSER validate

$DRUSH user-password admin $(openssl rand -base64 36)
$DRUSH config:set system.date timezone.default 'Europe/London'

if [ "$USE_SSL" == "yes" ] ; then
  certbot certonly -d $INTERNAL_NAME --apache -n
fi

# apache config
export SERVERSHORTNAME
export SITENAME
export INTERNAL_NAME
export USE_SSL
export EXTERNAL_NAME
j2 $SCRIPTDIR/templates/apache.j2 >$APACHECONFFILE
a2ensite $SERVERSHORTNAME
apache2ctl graceful

j2 $SCRIPTDIR/templates/drupal-settings.j2 >> web/sites/default/settings.php

# NB php_storage must be outside the webserver docroot, but still
# writeable by the webserver process.
for DIR in web/files web/assets php_storage; do
  mkdir $DIR
  chown www-data:www-data $DIR
  chmod g+s $DIR
done

mkdir config_sync
rm -r web/sites/default/files

if [ "${FLAVOUR}" == "minimal" ] ; then
  exit
fi

# Add repo for mmenu js library; needed by responsive_menu which is needed by pl9nav
$COMPOSER config repositories.frdh_mmenu_js '{"type": "package", "package": {"name": "frdh/mmenu-js", "version": "v8.5.24", "type": "drupal-library", "extra": {"installer-name": "mmenu"}, "dist": {"url": "https://github.com/FrDH/mmenu-js/archive/v8.5.24.zip","type": "zip"}, "require": {"composer/installers": "^2.0"}}}'

# Repos needed for views_slideshow and views_slideshow_cycle
$COMPOSER config repositories.briancherne/jquery.hover-intent '{"type": "package", "package": {"name": "briancherne/jquery.hover-intent", "version": "1.10.2", "type":"drupal-library", "dist": {"url": "https://github.com/briancherne/jquery-hoverIntent/archive/refs/tags/v1.10.2.tar.gz","type": "tar"}}}'
$COMPOSER config repositories.malsup/jquery.cycle '{"type": "package", "package": {"name": "malsup/jquery.cycle","version": "3.0.3-a","type":"drupal-library","dist": {"url": "https://github.com/malsup/cycle/archive/refs/tags/3.0.3-a.tar.gz","type": "tar"}}}'
$COMPOSER config repositories.douglascrockford/json2 '{"type": "package","package": {"name": "douglascrockford/json2","version": "dev-master","type":"drupal-library","source": {"url": "https://github.com/douglascrockford/JSON-js","type": "git","reference": "master"}}}'
$COMPOSER config repositories.tobia/jquery.pause '{"type": "package","package": {"name": "tobia/jquery.pause","version": "dev-master","type":"drupal-library","source": {"url": "https://github.com/tobia/Pause","type": "git","reference": "master"}}}'

# lirbaries required for views_slideshow and views_slideshow_cycle which we use for carousels
$COMPOSER require briancherne/jquery.hover-intent malsup/jquery.cycle douglascrockford/json2 tobia/jquery.pause

# Set the default theme.
# N.B. mmenu-js is a pure javascript library, not a Drupal module, so we just need to ensure
# composer pulls it in.
$COMPOSER require ucam_drupal/pl9 ucam_drupal/pl9nav frdh/mmenu-js ucam_drupal/pl9settings

# Enable modules that the pl9 theme depends upon.
# Enabling the theme does not seem to enable the depended-upon module despite
# https://www.drupal.org/node/2937955 so we'll do it first.
$DRUSH pm:install twig_real_content

$DRUSH theme:enable pl9
$DRUSH config:set system.theme default pl9
$DRUSH theme:uninstall stark

$DRUSH pm:install pl9nav
$DRUSH pm:install pl9settings

# authn/authz things
$COMPOSER require ucam_drupal/ucam_entra_authn ucam_drupal/linuxgroup_to_role
$DRUSH pm:install ucam_entra_authn linuxgroup_to_role
$DRUSH ucam_entra_authn:set-client-id $ENTRA_CLIENT_ID
/usr/local/ucam-app-secrets/bin/deploy-drupal-secrets -s $SITENAME
$DRUSH ucam_entra_authn:set-plugin-status enabled
# deny access to default drupal URLs for user/login and user/pass
$DRUSH ucam_entra_authn:allow-user-login-route no
$DRUSH ucam_entra_authn:allow-user-password-route no

# Ideally we would just redirect users from /user/login to our Entra
# login. Perhaps https://www.drupal.org/project/openid_connect/issues/3011413
# might be resolved and we can review this.
$DRUSH config:set openid_connect.settings user_login_display replace
# Redirect to homepage after logins
$DRUSH config:set openid_connect.settings redirect_login '<front>'
# Chemistry default role mappings
$DRUSH lgtr-addmap cosgroup administrator
$DRUSH lgtr-addmap chdept-webmasters content_editor
# add cam.ac.uk to the list of domains accepted by linuxgroup_to_role
$DRUSH lgtr-adddom cam.ac.uk
# ensure unmapped roles are removed from users
$DRUSH lgtr-rur true
# We set case_sensitive=false in sssd. This means that e.g. getent group 7005
# returns 'domain users' not 'Domain Users'. We thus want to perform
# case-insensitive matching of group names when assigning roles.
$DRUSH lgtr-cigm true

# Misc modules from drupal.org
$COMPOSER require drupal/diff:^1.8
$DRUSH pm:install diff

$COMPOSER require drupal/content_access:^2.0 ucam_drupal/content_access_audit:^1.0
$DRUSH pm:install content_access content_access_audit

# Chemistry modules
$COMPOSER require chem_drupal/chemistry_footer
$DRUSH pm:install chemistry_footer

$COMPOSER require chem_drupal/chemistry_microsite_breadcrumb
$DRUSH pm:install chemistry_microsite_breadcrumb

$COMPOSER require ucam_drupal/drush_entity_create
$DRUSH pm:install drush_entity_create

$COMPOSER require ucam_drupal/faq_page
$DRUSH pm:install faq_page

if [ "${FLAVOUR}" == "research" ] ; then
  $COMPOSER require chem_drupal/chemistry_staff_page chem_drupal/chemistry_staff_content_syncer # chem_drupal/elements_db_client

  $DRUSH pm:install chemistry_staff_page chemistry_staff_content_syncer
  # NB elements_db_client does not depend upon chemistry_staff_page, but perhaps
  # it should ...
  # $DRUSH pm:install elements_db_client

  # echo -e "\ninclude_once('/etc/drupal-chem/symplectic_elements.php');" >> $SETTINGSPHP
fi

$COMPOSER require ucam_drupal/entityqueue_blocks
$DRUSH pm:install entityqueue_blocks

$COMPOSER require chem_drupal/chemistry_googletag
$DRUSH pm:install chemistry_googletag

# Misc Chemistry settings. Deliberately at the end of the set of modules
# we deploy & enable: we might have this module attempt to deploy settings
# that assume/depend upon previous parts of this script.
$COMPOSER require chem_drupal/chemistry_defaults
$DRUSH pm:install chemistry_defaults

# quick-links is created in chemistry_defaults. Whilst we could install
# always_show_menuitems earlier, I'm keeping the installation and configuration
# steps together for ease of understanding.
$COMPOSER require ucam_drupal/always_show_menuitems
$DRUSH pm:install always_show_menuitems
# We want to show users links in these menus even if they do not have access to
# the linked content. We then redirect users to login if they encounter a 403.
$DRUSH config:set -y --input-format=yaml always_show_menuitems.settings always_show_on_menus "[main,quick-links]"

# Set up the home page
$DRUSH entity:create node "Home" --bundle page
$DRUSH config:set system.site page.front "/node/1"

# Address used if the Drupal install needs to send emails
$DRUSH config:set system.site mail support@ch.cam.ac.uk

# cron settings
# disable detailed cron logging
$DRUSH config:set system.cron logging 0

# Set file_managed statuses to temporary when there are no more references,
# and delete on next cron run if at least an hour has elapsed.
# Also see https://www.drupal.org/project/drupal/issues/2821423
$DRUSH config:set file.settings make_unused_managed_files_temporary true
$DRUSH config:set system.file temporary_maximum_age 3600

# e.g. chemistry_defaults provides content_access settings that flag a
# rebuild. The docs say we should not call this from hook_install(); I
# don't know why not but let's defer to them and do it here as part of
# final tidy-up.
$DRUSH php:eval "node_access_rebuild();"

# clearing caches might not be necessary at this point but it seems like
# a reasonable thing to do!
$DRUSH cr

# suppress any initial warning that cron has never run.
$DRUSH cron
